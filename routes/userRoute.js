import express from "express";
import { getAllUsers } from "../controllers/userController.js";

const userRoute = express.Router();

userRoute.route("/").get(getAllUsers);
export default userRoute;
