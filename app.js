import express from "express";
// import userRoute from "./routes/userRoute.js";
// import nodemailer from "nodemailer";

// const tranport = nodemailer.createTransport({
//   service: "gmail",
//   auth: {
//     user: "os.zero.one.2021@gmail.com",
//     pass: "bagtisypdzqavpky",
//   },
// });
// const mailOptions = {
//   from: "Eng alaa Asaad",
//   to: "alaa.asaad.zero.one@gmail.com",
//   subject: "this is from my node Mailer server",
//   text: "powerful and dedicated , carrer , collabrative",
// };

// tranport.sendMail(mailOptions, (error, info) => {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log("Emait sent" + info.response);
//   }
// });

const app = express();
// // app.use(express.json());
// // app.use("/api/v1/users", userRoute);

/**
 * Let us create a function that receives a string "abcbdbd",
 * and returns an array like:
["a", "a.b", "a.b.c", "a.b.c.b", "a.b.c.b.d", "a.b.c.b.d.b", ...]
 */

const splitString = (str) => {
  // console.log(str);
  let a = [...str];
  let res = [];
  let finres = [];

  for (let i = 0; i < str.length; i++) {
    console.log([...str.slice(0, i + 1)].join("."));
    finres.push([...str.slice(0, i + 1)].join("."));
  }

  return finres;
};
const splitString2 = (str) =>{
  console.log( typeof str);
  const resArray = [...str];
  console.log(resArray);
  const finalArray = resArray.map((__,index,origin)=> {
    return origin.slice(0,index+1);
  })
  return finalArray;
}

// console.log("str", splitString2("abcdef"));

// PlainDom  radar , racecar , aaa, bbb
/**
 * Have the function palindrome(str) take the str parameter being passed and
 * return the string true if the parameter is a palindrome, (the string is the
 * same forward as it is backward) otherwise return the string false. For
 * example: "racecar" is also "racecar" backwards. Punctuation and numbers will
 * not be part of the string.**/

const palindrome = (str) => {
  let a = [...str];
  let aReverse = [...str];
  aReverse.reverse();
  let truthArray = [];
  let res;
  console.log("a", a);
  console.log("reverse a", aReverse);
  a.map((item, index) => {
    if (item === aReverse[index]) {
      truthArray.push(true);
    } else {
      truthArray.push(false);
    }
  });
  console.log("truth", truthArray);
  let finRes = truthArray.some((i) => i === false);

  return !finRes;
};
const palindrome2= (str) =>{
  
  const a = [...str];
  console.log(a);
  let min=0;
  let max= a.length - 1;
  while(min < max){
    if(a[min]!== a[max]){
      return false;
    }
    min++;
    max--;
  }

  return true;
}
console.log("palindrome", palindrome2("racecar"));

// //Arra Addition
// * Have the function arrayAdditionI(arr) take the array of numbers stored in arr
//  * and return the string true if any combination of numbers in the array
// * (excluding the largest number) can be added up to equal the largest number in
// * the array, otherwise return the string false. For example: if arr contains
// * [4, 6, 23, 10, 1, 3] the output should return true because 4 + 6 + 10 + 3 =
// * 23. The array will not be empty, will not contain all the same elements, and
// * may contain negative numbers.
const arrayAddition = (arr) => {
  console.log("original", arr);
  let a = [...arr];
  console.log(
    "sorted",
    a.sort((a, b) => a - b)
  );
  let max = a[a.length - 1];
  let sum = 0;
  a.map((item, index) => {
    if (index !== a.length - 1) {
      sum += item;
    }
  });
  console.log("max", max);
  console.log("sum", sum);
  return sum >= max ? true : false;
};
// console.log("ArrayAddition", arrayAddition([4, 6, 230, 10, 1, 3]));

// //Arra Addition but in best practice ,best soultion
// * Have the function arrayAdditionI(arr) take the array of numbers stored in arr
//  * and return the string true if any combination of numbers in the array
// * (excluding the largest number) can be added up to equal the largest number in
// * the array, otherwise return the string false. For example: if arr contains
// * [4, 6, 23, 10, 1, 3] the output should return true because 4 + 6 + 10 + 3 =
// * 23. The array will not be empty, will not contain all the same elements, and
// * may contain negative numbers.
const arrayAdditionBest = (arr) => {
  console.log(arr);
  let low = 0;
  let high = arr.length - 1;
  let max = 0;
  let remain = 0;
  console.log(low, high);
  while (low < high) {
    if (arr[low] > arr[high]) {
      max = arr[low];
      remain += arr[high];
      high -= 1;
    } else {
      max = arr[high];
      remain += arr[low];
      low += 1;
    }
  }
  console.log("max", max);
  console.log("remain", remain);
  return remain >= max ? true : false;
};

// console.log("ArrayAddition", arrayAdditionBest([4, 6, 23, 10, 1, 3]));

//// Tic-TOC-TAC problem , if you have two deminsional array , and have 3 of 1's or 3 of 0's
// determine the winner if there 3 of 1 or 3 of 0 horizan or vertically or anloglay ,
//  like [[0,null,1],[0,null,1],[0,null,null]] here the winner is 0 cus it have in first
//  column 3 of 0           0 null 1
//                          0 null 1
//                          0 null null

const ticTocTac = (arr, target) => {
  let sqr = arr.flat();
  console.log(sqr);
  if (((sqr[8] == sqr[7]) == sqr[6]) == target) {
    console.log("0 is the winner");
    return target + " is the winner";
  }
};
// console.log(
//   "TicTacToc",
//   ticTocTac(
//     [
//       [null, null, null],
//       [1, 0, 1],
//       [0, 0, 0],
//     ],
//     0
//   )
// );

// Arith Geo
// * Have the function arithGeo(arr) take the array of numbers stored in arr and
//  * return the string "Arithmetic" if the sequence follows an arithmetic pattern
//  * or return "Geometric" if it follows a geometric pattern. If the sequence
//  * doesn't follow either pattern return -1. An arithmetic sequence is one where
//  * the difference between each of the numbers is consistent, where as in a
//  * geometric sequence, each term after the first is multiplied by some constant
//  * or common ratio. Arithmetic example: [2, 4, 6, 8] and Geometric example: [2,
//  * 6, 18, 54]. Negative numbers may be entered as parameters, 0 will not be
//  * entered, and no array will contain all the same elements.

const arithGeo = (arr) => {
  if (arr.length === 1) {
    return -1;
  }
  console.log(arr);
  let a = [...arr];
  let geo = [];
  let basedif = arr[1] - arr[0];
  let dif = [];
  // a.sort((a, b) => a - b);
  a.map((item, index) => {
    if (index < arr.length - 1) {
      let next = a[index + 1];
      let arthDif = next - item;
      if (next % item === 0) {
        geo.push(true);
      } else {
        geo.push(false);
      }
      if (arthDif === basedif) {
        dif.push(true);
      } else {
        dif.push(false);
      }
    }
  });
  // console.log("dif", dif);
  console.log("arth", dif);

  console.log("geo", geo);
  let findDif = dif.some((i) => i === false);
  let findGeo = geo.some((i) => i === false);
  if (!findDif) {
    return "Arithmetic";
  }
  if (!findGeo) {
    return "Geometric";
  } else {
    return -1;
  }
};

// console.log("arithGeo", arithGeo([2, 4, 8, 16]));

/**
 * Have the function vowelCount(str) take the str string parameter being passed
 * and return the number of vowels the string contains (ie. "All cows eat grass
 * and moo" would return 8). Do not count y as a vowel for this challenge.
 * @param  {string} str
 * @return {number}
 */

const countVowel = (str) => {
  let vowel = ["a", "i", "o", "u", "e", "A", "I", "O", "U", "E"];
  let a = [...str];
  let count = 0;
  let countConstant = 0;
  let b = a.map((item, index) => {
    if (vowel.some((i) => item === i)) {
      count += 1;
    } else {
      countConstant += 1;
    }
  });
  console.log(count);
  console.log(countConstant);
  return count;
};

// console.log("vowel", countVowel("All cows eat grass and moo"));

/**
 * Have the function wordCount(str) take the str string parameter being passed
 * and return the number of words the string contains (e.g. "Never eat shredded
 * wheat or cake" would return 6). Words will be separated by single spaces.
 * @param  {string} str
 * @return {number}
 */
const wordCount = (str) => {
  if (str.length === 0) {
    return 0;
  }

  let a = str.trim();
  let b = [];
  b = a.split(" ");
  console.log(b);
  if (b[0] === "") {
    return 0;
  }
  return b.length;
};
// console.log("WOrd Count ", wordCount(" gdfgd dfger"));

/**
 * Have the function timeConvert(num) take the num parameter being passed and
 * return the number of hours and minutes the parameter converts to (ie. if num
 * = 63 then the output should be 1:3). Separate the number of hours and minutes
 * with a colon.
 * @param  {number} num
 * @return {string} number of hours and minutes.  hours:minutes
 */

const timeConvert = (num) => {
  let hours = 0;
  let min = 0;
  min = num % 60;
  hours = (num - min) / 60;
  console.log("min", min);
  console.log("hours", hours);
  return `${hours}:${min}`;
};
// console.log("timeConvert", timeConvert(245));

/**
 * Have the function thirdGreatest(strArr) take the array of strings stored in
 * strArr and return the third largest word within in. So for example: if strArr
 * is ["hello", "world", "before", "all"] your output should be world because
 * "before" is 6 letters long, and "hello" and "world" are both 5, but the
 * output should be world because it appeared as the last 5 letter word in the
 * array. If strArr was ["hello", "world", "after", "all"] the output should be
 * after because the first three words are all 5 letters long, so return the
 * last one. The array will have at least three strings and each string will
 * only contain letters.
 *
 *
 * @param  {array} strArr
 * @return {string}
 */

const thirdGreatest = (arr) => {
  let a = [...arr];
  let count = [];
  console.log(a);
  a.map((item, index) => count.push({ index: index, length: item.length }));
  count.sort((a, b) => b.length - a.length);
  console.log(count);
  let wantedLength = count[2].length;
  let wantedIndex = count[2].index;

  count.map((item) => {
    if (item.length === wantedLength) {
      wantedIndex = item.index;
    }
  });
  return arr[wantedIndex];
};

// console.log("thirdGreatest", thirdGreatest(["hello", "world", "after", "all"]));

// IF YOU WANT TO SORT ARRAY OF OBJECTS
// a.map((item, index) => {
//   count.push({ index: index, length: item.length });
// });

// count.sort((a, b) => a.length - b.length);

/**
 * Have the function swapCase(str) take the str parameter and swap the case of
 * each character. For example: if str is "Hello World" the output should be
 * hELLO wORLD. Let numbers and symbols stay the way they are.
 *
 *
 * @param  {string} str
 * @return {string}
 */
const swapCase = (str) => {
  let a = [...str];
  let b = [];
  let newb = [];
  console.log(a);
  a.map((item, index) => {
    b.push(item.charCodeAt());
  });
  console.log("b", b);
  b.map((item) => {
    if (item >= 97 && item <= 122) {
      newb.push(String.fromCharCode(item - 32));
    } else if (item >= 65 && item <= 90) {
      newb.push(String.fromCharCode(item + 32));
    } else {
      newb.push(String.fromCharCode(item));
    }
  });

  console.log("new b", newb);

  return newb.join("");
};

// console.log("swapcase", swapCase("the QUICK Brown foX"));

/**
 * Have the function simpleSymbols(str) take the str parameter being passed and
 * determine if it is an acceptable sequence by either returning the string true
 * or false. The str parameter will be composed of + and = symbols with several
 * letters between them (ie. ++d+===+c++==a) and for the string to be true each
 * letter must be surrounded by a + symbol. So the string to the left would be
 * false. The string will not be empty and will have at least one letter.
 *
 *
 * @param  {string} str
 * @return {string} 'true' or 'false'
 */
const simpleSymbols = (str) => {
  if (str.length < 3) {
    return "false";
  }
  let a = [...str];
  let b = [];
  let newb;

  a.map((item) => {
    b.push(item.charCodeAt());
  });
  if (b[0] >= 97 && b[0] <= 122) {
    return "false";
  } else if (b[str.length - 1] >= 97 && b[str.length - 1] <= 122) {
    return "false";
  } else {
    newb = b.map((item, index, arr) => {
      if (index !== str.length - 1) {
        if (
          item !== 43 &&
          item !== 61 &&
          (arr[index - 1] !== 43 || arr[index + 1] !== 43)
        ) {
          return false;
        } else if (
          item !== 43 &&
          item !== 61 &&
          arr[index - 1] === 43 &&
          arr[index + 1] === 43
        ) {
          return true;
        }
      }
    });
  }
  console.log(b);
  console.log(newb);

  let NotallSympole = newb.some((i) => i === true);
  if (!NotallSympole) {
    return "false";
  }
  let finre = newb.some((i) => i === false);

  let finaleres = !finre;
  return finaleres.toString();
};
// console.log("simpleSymbole", simpleSymbols("++d+===+c++==+a+"));

/**
 * Have the function simpleAdding(num) add up all the numbers from 1 to num. For
 * example: if the input is 4 then your program should return 10 because 1 + 2 +
 * 3 + 4 = 10. For the test cases, the parameter num will be any number from 1
 * to 1000.
 * @param  {number} num
 * @return {number}
 */
const simpleAdding = (num) => {
  if (num === 1) {
    return 1;
  }
  return num + simpleAdding(num - 1);
};

// console.log("simpleAdding", simpleAdding(4));

/**
 * Have the function secondGreatLow(arr) take the array of numbers stored in arr
 * and return the second lowest and second greatest numbers, respectively,
 * separated by a space. For example: if arr contains [7, 7, 12, 98, 106] the
 * output should be 12 98. The array will not be empty and will contain at least
 * 2 numbers. It can get tricky if there's just two numbers!
 *
 *
 * @param  {array} arr
 * @return {string}
 */
const secondGreatLow = (arr) => {
  let slowest;
  let sGreatest;
  let a = [...arr];
  a.sort((a, b) => a - b);
  console.log(a.length);
  if (a.length <= 2) {
    return `${a[0]} ${a[1]}`;
  }
  let b = a.filter((value, index, arra) => !arra.includes(value, index + 1));

  console.log("a", a);
  console.log("b", b);
  slowest = b[1];
  sGreatest = b[b.length - 2];
  return `${slowest} ${sGreatest}`;
};

// console.log("sceondGreatLow", secondGreatLow([7, 7, 7, 12, 12, 10, 98, 106]));

/**
 * Have the function powersOfTwo(num) take the num parameter being passed which
 * will be an integer and return the string true if it's a power of two. If it's
 * not return the string false. For example if the input is 16 then your program
 * should return the string true but if the input is 22 then the output should
 * be the string false.
 *
 *
 * @param  {number} num
 * @return {string} 'true' or 'false'
 */
const powersOfTwo = (num) => {
  // the number is power of 2 if have only and only one bit of 1  in binary code
  console.log((num >>> 0).toString(2)); // transfer any number to binary
  let number = (num >>> 0).toString(2);
  let a = [...number];
  console.log("a", a);
  let res = [];
  a.map((i) => {
    if (i == 1) {
      res.push(i);
    }
  });

  return res.length === 1 ? "true" : "fasle";
};

// console.log("powersOfTwo", powersOfTwo(1024));

/**
 * Have the function prime(num) take the num parameter being passed which
 * will be an integer and return the string true if it's num is prime.
 * the prime number is not 1  , and the factor of the number is 1 and iteself
 * like 2,3,5,7,11,13,17
 *
 *
 * @param  {number} num
 * @return {string} 'true' or 'false'
 */
const primeNum = (num) => {
  // IF num like 20 22 24 26 28  end with 0,2,4,6,8 is not prime
  // IF num end with 5  is not prime
  // If num sum like 15 the sum 1+5 = 6 is divided by 3 is not prime
  if (num === 1 || num <= 0) {
    return "false";
  }
  let number = num.toString();
  let a = [...number];
  let b = a.map((item) => +item);
  console.log(b);
  let sum = b.reduce((a, b) => a + b);
  console.log(sum);
  if (b[b.length - 1] % 2 === 0 || sum % 3 === 0) {
    console.log("hello improve");
    return "false";
  }

  for (let i = 2; i < num; i++) {
    if (num % i === 0) {
      return "false";
    }
  }
  return "true";
};

// console.log("Prime Number", primeNum(9));
/**
 * Have the function numberAddition(str) take the str parameter, search for all
 * the numbers in the string, add them together, then return that final number.
 * For example: if str is "88Hello 3World!" the output should be 91. You will
 * have to differentiate between single digit numbers and multiple digit numbers
 * like in the example above. So "55Hello" and "5Hello 5" should return two
 * different answers. Each string will contain at least one letter or symbol.
 *
 *
 * @param  {string} str
 * @return {number}
 */
const numberAddition = (str) => {
  //  Anwsr 1
  // console.log("reg", str.match(/\d+/g));
  // let a = [...str.match(/\d+/g)];
  // let b = a.reduce((a, b) => +a + +b);
  // return b;
  // -------------------------------------
  //  Anwsr 2
  let digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  let a = [...str];
  let b = [];
  let nump = "";
  a.map((item, index) => {
    if (!digits.includes(item)) {
      if (nump != "") {
        b.push(nump);
      }
      nump = "";
    } else {
      nump += item;
      if (index == a.length - 1) {
        b.push(nump);
      }
    }
  });
  return b.reduce((a, b) => +a + +b);
};
// console.log("nubmerAddition", numberAddition("55Hello 3  5h"));

// give a an array return array in whish element has the products of all remains elemnts ,
// like [1,2,3,4] >>>> [24,12,8,6]

const productOfRestElemnts = (arr) => {
  let a = [...arr];
  let c = [];
  a.map((item, index, arra) => {
    let prod = 1;
    arra.map((i, ind) => {
      if (index !== ind) {
        prod = prod * i;
      }
    });
    c.push(prod);
  });
  // console.log(c);
  return c;
};
// console.log("productOfRestElemnts", productOfRestElemnts([2, 2, 2, 2]));

/**
 * Have the function meanMode(arr) take the array of numbers stored in arr and
 * return 1 if the mode equals the mean, 0 if they don't equal each other (ie.
 * [5, 3, 3, 3, 1] should return 1 because the mode (3) equals the mean (3)).
 * The array will not be empty, will only contain positive integers, and will
 * not contain more than one mode.
 *
 *
 * @param  {array} arr
 * @return {number}
 */
const meanMode = (arr) => {
  console.log(arr);
  let sum = arr.reduce((a, b) => a + b);
  let mean = sum / arr.length;
  console.log(mean);
  let a = [...arr];
  let b = a.map((item, index, arra) => {
    let apperance = 0;
    arra.map((i, ind) => {
      if (i === item) {
        apperance += 1;
      }
    });
    return apperance;
  });
  let max = 0;
  for (let i = 0; i <= b.length - 1; i++) {
    if (b[i] >= max) {
      max = i;
    }
  }

  let mode = arr[max];
  console.log(mode);
  return mean === mode ? 1 : 0;
};
// console.log("meanMode", meanMode([5, 3, 3, 3, 10]));

/**
 * Have the function longestWord(sen) take the sen parameter being passed and
 * return the largest word in the string. If there are two or more words that
 * are the same length, return the first word from the string with that length.
 * Ignore punctuation and assume sen will not be empty.
 * @param  {string} sen
 * @return {string}
 */
//

const longestWord = (str) => {
  let strA = str.replace(/[$%!@#^_&*0.()-=+{}<>"|~/]/g, " ");
  console.log(strA);
  const a = strA.split(" ");
  let b = [];
  b = a.map((item, index) => {
    return item.length;
  });
  let max = 0;
  let maxIndex = 0;
  for (let i = 0; i <= b.length - 1; i++) {
    if (b[i] > max) {
      max = b[i];
      maxIndex = i;
    }
  }
  console.log(a);

  console.log(b);
  return a[maxIndex];
};
// console.log(
//   "longestWord",
//   longestWord("the$%!#$.quick*brown jump*!#$!@$!$!!%!00an")
// );

// given an Array of number see if the same of pair of them is equal to given sum
//  like [1,2,4,4] ,sum = 8 it should return true , [3,1,6,6] ,sum = 8 it should return false

const pairOfNumber = (arr, sum) => {
  console.log(arr, sum);
  let a = [...arr];
  let inds = [];
  let intSum;
  a.map((item, index, arra) => {
    arra.map((i, ind) => {
      if (ind !== index) {
        intSum = item + i;
        if (intSum === sum) {
          inds.push(ind);
        }
      }
    });
  });
  console.log(inds);
  return inds.length === 0 ? false : true;
};
// console.log("pairOfNumber", pairOfNumber([1, 2, 3, 4], 8));

/**
 * Have the function letterCountI(str) take the str parameter being passed and
 * return the first word with the greatest number of repeated letters. For
 * example: "Today, is the greatest day ever!" should return greatest because it
 * has 2 e's (and 2 t's) and it comes before ever which also has 2 e's. If there
 * are no words with repeating letters return -1. Words will be separated by
 * spaces.
 *
 *
 * @param  {string} str
 * @return {string} or -1
 */
const letterCount = (str) => {
  if (str === "") {
    return -1;
  }
  console.log(str);
  let a = str.split(" ");
  console.log("a", a);
  let counters = a.map((item, index, arra) => {
    let count = 0;
    let repeatedLetters = [];
    for (let i = 0; i <= item.length - 1; i++) {
      for (let j = 0; j <= item.length - 1; j++) {
        if (i !== j) {
          if (item[i] === item[j] && !repeatedLetters.includes(item[j])) {
            count += 1;
            repeatedLetters.push(item[i]);
          }
        }
      }
    }

    return count;
  });
  // if no repated letters return -1 ,hello
  let thereIsRepatedLetters = counters.some((i) => i !== 0);
  if (!thereIsRepatedLetters) {
    return -1;
  }
  let max = counters.reduce((a, b) => Math.max(a, b));
  let indexMax = counters.findIndex((i) => i === max);

  console.log("Counters", counters);
  return a[indexMax];
};
// console.log("letterCount", letterCount("Today, is the greatest day ever!"));

/**
 * Have the function letterChanges(str) take the str parameter being passed and
 * modify it using the following algorithm. Replace every letter in the string
 * with the letter following it in the alphabet (ie. c becomes d, z becomes a).
 * Then capitalize every vowel in this new string (a, e, i, o, u) and finally
 * return this modified string.
 * @param  {string} str
 * @return {string}
 *
 *  SAme As caesarCipher()
 */

const letterChanges = (str) => {
  if (str === "") {
    return " ";
  }
  console.log(str);
  let vowel = [97, 101, 105, 111, 117];
  let a = [...str];
  let b = [];
  let c = [];
  a.map((item) => {
    b.push(item.charCodeAt());
  });
  b.map((item) => {
    if ((item >= 65 && item < 90) || (item >= 97 && item < 122)) {
      c.push(item + 1);
    } else if (item === 90 || item === 122) {
      c.push(item - 25);
    } else {
      c.push(item);
    }
  });
  let finres = [];
  c.map((item) => {
    if (vowel.includes(item)) {
      finres.push(String.fromCharCode(item - 32));
    } else {
      finres.push(String.fromCharCode(item));
    }
  });

  // console.log(a);
  // console.log(b);
  // console.log(c);
  // console.log(finres);
  return finres.join("");
};

// console.log("LetterChange", letterChanges("shifts letters by given key"));

/**
 * Have the function letterCapitalize(str) take the str parameter being passed
 * and capitalize the first letter of each word. Words will be separated by only
 * one space.
 * @param  {string} str
 * @return {string}
 */
const letterCapitalize = (str) => {
  if (str === "") {
    return "empty string";
  }
  console.log(str);
  let a = str.split(" ");
  console.log(a);
  let b = a.map((item, index) => {
    let word = "";
    for (let i = 0; i <= item.length - 1; i++) {
      if (i === 0) {
        word += item[0].toUpperCase();
      } else {
        word += item[i];
      }
    }
    return word;
  });
  return b.join(" ");
};
// console.log("letterChanges", letterCapitalize("the quick brown fox jumped"));

// Given a non-empty array of integers nums, every element appears twice except for one.
//  Find that single one.
// You must implement a solution with a linear runtime complexity and
//  use only constant extra space.
/**
 * @param {number[]} nums
 * @return {number}
 */
const singleNumber = (arr) => {
  // console.log(arr);
  // console.log(
  //   "sorting",
  //   arr.sort((a, b) => a - b)
  // );
  // let found = [];
  // for (let i = 0; i <= arr.length - 1; i++) {
  //   for (let j = 0; j <= arr.length - 1; j++) {
  //     if (i !== j) {
  //       if (arr[i] === arr[j] && !found.includes(arr[j])) {
  //         found.push(arr[i]);
  //         break;
  //       }
  //     }
  //   }
  //   if (!found.includes(arr[i])) {
  //     return arr[i];
  //   }
  // }
  // OPTIMAL SOULTION IS USING XOR FOR EACH ELEMENT OF THE ARRAY AND START WITH ZEOR
  let result = 0;
  for (let i = 0; i <= arr.length - 1; i++) {
    result ^= arr[i];
  }
  return result;
};
// console.log("singleNumber", singleNumber([2, 1, 2, 1, 5, 5, 3]));
/////// MEDUIM CHALLENGE
/**
 * Using the JavaScript language, have the function arrayMinJumps(arr) take the
 * array of integers stored in arr, where each integer represents the maximum
 * number of steps that can be made from that position, and determine the least
 * amount of jumps that can be made to reach the end of the array. For example:
 * if arr is [1, 5, 4, 6, 9, 3, 0, 0, 1, 3] then your program should output the
 * number 3 because you can reach the end of the array from the beginning via
 * the following steps: 1 -> 5 -> 9 -> END or 1 -> 5 -> 6 -> END. Both of these
 * combinations produce a series of 3 steps. And as you can see, you don't
 * always have to take the maximum number of jumps at a specific position, you
 * can take less jumps even though the number is higher.
 *
 * If it is not possible to reach the end of the array, return -1.
 *
 *
 * @param  {array} arr of integers
 * @return {number}
 */

const arrayMinJumps = (arr) => {
  if (arr[0] === 0) {
    return -1;
  }
  console.log(arr.length);
  let jumpsNeedToFinish = arr.length;
  let jumps = 1;
  console.log(jumpsNeedToFinish);
  let a = [...arr];
  let b = [];
  let arrayMax = [];
  a.map((item, index, arra) => {
    if (item === 0) {
      b.push([0]);
    } else {
      b.push(arra.slice(index + 1, index + item + 1));
    }
  });
  console.log("b", b);
  b.map((item, index) => {
    item.sort((a, b) => a - b);
  });
  b.map((item) => {
    if (item[item.length - 1] !== undefined) {
      arrayMax.push(item[item.length - 1]);
    }
  });
  console.log("b", b);
  console.log("arrayMax", arrayMax);
  let sumofMax = 0;
  sumofMax = arrayMax.reduce((a, b) => a + b);
  console.log("sumofMax", sumofMax);
  if (sumofMax < jumpsNeedToFinish) {
    return -1;
  }
  for (let i = 0; i <= arrayMax.length; i++) {
    if (arrayMax[i] + arrayMax[i + 1] >= jumpsNeedToFinish) {
      jumps += 1;
      let reach = i + 1;
      if (reach !== arrayMax.length - 1) {
        jumps += 1;
      }
      break;
    } else if (arrayMax[i] + arrayMax[i + 1] < jumpsNeedToFinish) {
      jumps += 1;
    }
  }
  return jumps;
};
// console.log(
//   "Meduim challenge arrayMinJumps",
//   arrayMinJumps([3, 4, 2, 1, 1, 100])
// );

export default app;
