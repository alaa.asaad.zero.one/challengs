/**
 * Using the JavaScript language, have the function arrayMinJumps(arr) take the
 * array of integers stored in arr, where each integer represents the maximum
 * number of steps that can be made from that position, and determine the least
 * amount of jumps that can be made to reach the end of the array. For example:
 * if arr is [1, 5, 4, 6, 9, 3, 0, 0, 1, 3] then your program should output the
 * number 3 because you can reach the end of the array from the beginning via
 * the following steps: 1 -> 5 -> 9 -> END or 1 -> 5 -> 6 -> END. Both of these
 * combinations produce a series of 3 steps. And as you can see, you don't
 * always have to take the maximum number of jumps at a specific position, you
 * can take less jumps even though the number is higher.
 *
 * If it is not possible to reach the end of the array, return -1.
 *
 *
 * @param  {array} arr of integers
 * @return {number}
 */

const arrayMinJumps = (arr) => {
  if (arr[0] === 0) {
    return -1;
  }
  console.log(arr.length);
  let jumpsNeedToFinish = arr.length;
  let jumps = 1;
  console.log(jumpsNeedToFinish);
  let a = [...arr];
  let b = [];
  let arrayMax = [];
  a.map((item, index, arra) => {
    if (item === 0) {
      b.push([0]);
    } else {
      b.push(arra.slice(index + 1, index + item + 1));
    }
  });
  console.log("b", b);
  b.map((item, index) => {
    item.sort((a, b) => a - b);
  });
  b.map((item) => {
    if (item[item.length - 1] !== undefined) {
      arrayMax.push(item[item.length - 1]);
    }
  });
  console.log("b", b);
  console.log("arrayMax", arrayMax);
  let sumofMax = 0;
  sumofMax = arrayMax.reduce((a, b) => a + b);
  console.log("sumofMax", sumofMax);
  if (sumofMax < jumpsNeedToFinish) {
    return -1;
  }
  for (let i = 0; i <= arrayMax.length; i++) {
    if (arrayMax[i] + arrayMax[i + 1] >= jumpsNeedToFinish) {
      jumps += 1;

      break;
    } else if (arrayMax[i] + arrayMax[i + 1] < jumpsNeedToFinish) {
      jumps += 1;
    }
  }
  return jumps;
};
console.log("arrayMinJumps", arrayMinJumps([1, 5, 4, 6, 9, 3, 0, 0, 1, 3]));
